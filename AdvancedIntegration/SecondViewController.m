//
//  SecondViewController.m
//  AdvancedIntegration
//
//  Created by Michael on 16/02/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()
@property (weak) AATKitAdManager *sharedAdManager;
@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.sharedAdManager = [AATKitAdManager sharedInstanceForController:self];
}


- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.sharedAdManager.delegate = self;
    [self.sharedAdManager showBannerOnViewController: self];
}

- (void) AATKitPauseForAd {
    NSLog(@"ad in fullscreen gets presented. You can pause you app here.");
}

- (void) AATKitResumeAfterAd {
    NSLog(@"The user has close the ad, or it has been dismissed automatically. You can resume your app here.");
}

- (void) AATKitHaveAd:(id) placement {
    NSLog(@"Banner ads get displayed automatically, fullscreen ads have to be triggered.");
}

- (void) AATKitNoAds:(id)placement {
    NSLog(@"The AATKit couldn't load an ad.");
}

@end
