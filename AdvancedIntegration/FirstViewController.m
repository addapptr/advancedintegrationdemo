//
//  FirstViewController.m
//  AdvancedIntegration
//
//  Created by Michael on 16/02/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()
@property (weak) AATKitAdManager *sharedAdManager;
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.sharedAdManager = [AATKitAdManager sharedInstanceForController: self];
    [self.sharedAdManager createBannerPlacement];
    [self.sharedAdManager startAutoLoadingBannerAds];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.sharedAdManager.delegate = self;
    [self.sharedAdManager showBannerOnViewController: self];
}

- (void) AATKitPauseForAd {
    NSLog(@"ad in fullscreen gets presented. You can pause your app here.");
}

- (void) AATKitResumeAfterAd {
    NSLog(@"The user has close the ad, or it has been dismissed automatically. You can resume your app here.");
}

- (void) AATKitHaveAd:(id) placement {
    NSLog(@"Banner ads get displayed automatically, fullscreen ads have to be triggered.");
}

- (void) AATKitNoAds:(id)placement {
    NSLog(@"The AATKit couldn't load an ad.");
}
@end
