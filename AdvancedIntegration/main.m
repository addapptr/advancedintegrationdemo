//
//  main.m
//  AdvancedIntegration
//
//  Created by Michael on 16/02/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
