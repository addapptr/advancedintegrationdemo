//
//  AATKitAdManager.h
//  AdvancedIntegration
//
//  Created by Michael on 16/02/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AATKit/AATKit.h>


@interface AATKitAdManager : NSObject <AATKitDelegate>
@property (weak, nonatomic) id<AATKitDelegate> delegate;
+ (AATKitAdManager *)sharedInstanceForController:(UIViewController*) viewCon;

- (void) createBannerPlacement;
- (void) startAutoLoadingBannerAds;
- (void) showBannerOnViewController: (UIViewController*) viewCon;
- (void) updateViewController: (UIViewController*) viewCon;
@end
