//
//  AATKitAdManager.m
//  AdvancedIntegration
//
//  Created by Michael on 16/02/16.
//  Copyright © 2016 AddApptr. All rights reserved.
//

#import "AATKitAdManager.h"

NSString *const BANNER_PLACEMENT_NAME = @"first_banner_placement";

@interface AATKitAdManager()
@property (weak, nonatomic) UIViewController *presentingViewController;
@property (strong, nonatomic) id bannerPlacement;
@property (nonatomic, assign) BOOL bannerIsDisplayed;
@end

@implementation AATKitAdManager

+ (AATKitAdManager *)sharedInstanceForController:(UIViewController*) viewCon
{
    static AATKitAdManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] initWithViewController:viewCon];
    });
    [AATKit setViewController:viewCon];
    sharedInstance.presentingViewController = viewCon;
    return sharedInstance;
}


- (instancetype)initWithViewController: (UIViewController*) viewCon
{
    self = [super init];
    if (self) {
        [AATKit extendedDebug: NO];
        [AATKit debug: YES];
        //Remove this line before publishing your app!!!
        [AATKit initWithViewController:viewCon delegate:self andEnableTestModeWithID:154];
        self.presentingViewController = viewCon;
        self.bannerIsDisplayed = NO;
    }
    return self;
}

- (void) createBannerPlacement {
    //create the banner size according to your user's device.
    self.bannerPlacement = [AATKit createPlacementWithName: BANNER_PLACEMENT_NAME andType:AATKitBanner414x53];
    [AATKit setPlacementAlign:AATKitBannerCenter forPlacement: self.bannerPlacement];
}

- (void) startAutoLoadingBannerAds {
    [AATKit startPlacementAutoReload:self.bannerPlacement];
}

/*For demonstration purposes, the banner view's background is set to grey.
 Currently the standard banner sizes > 320, for iPhone 6 and 6 Plus aren't provided.
 Hence, specifying a banner size of 414x53, a container for banners is created
 with exactly these values, while the banner contained will have 320x53. This
 leaves a margin on the right and left hand side. To illustrate this, the banner
 view's background is set to grey.*/
- (void) showBannerOnViewController: (UIViewController*) viewCon {
    self.presentingViewController = viewCon;
    if (self.bannerIsDisplayed) {
        [self positionBannerOnTopOfTabbar];
    } else {
        [self positionBannerBelowViewController];
    }
}


- (void) updateViewController: (UIViewController*) viewCon{
    [AATKit setViewController: viewCon];
}

- (void) updateDelegate: (id<AATKitDelegate>) newDelegate {
    self.delegate = newDelegate;
}

- (void) animateBannerView {
    [self positionBannerBelowViewController];
    [UIView animateWithDuration: 0.5
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         [self positionBannerOnTopOfTabbar];
                         self.bannerIsDisplayed = YES;
                     } completion: nil];
}

- (void) positionBannerOnTopOfTabbar {
    UIView *bannerView = [AATKit getPlacementView: self.bannerPlacement];
    CGFloat viewControllerHeight = self.presentingViewController.view.frame.size.height;
    CGFloat bannerHeight = bannerView.frame.size.height;
    CGPoint pos = CGPointMake(0, viewControllerHeight - bannerHeight);
    bannerView.backgroundColor = [UIColor grayColor];
    NSLog(@"banner x: %f, banner y: %f", bannerView.center.x, bannerView.center.y);
    [AATKit setPlacementPos: pos
               forPlacement: self.bannerPlacement];
    NSLog(@"banner x: %f, banner y: %f", bannerView.center.x, bannerView.center.y);
    // make the banner view stay there even when superview size changes.
    bannerView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin;
    [self.presentingViewController.view addSubview: bannerView];
    [self.presentingViewController.view bringSubviewToFront: bannerView];

}

- (void) positionBannerBelowViewController {
    NSLog(@"Window height: %f", [[UIScreen mainScreen] bounds].size.height);
    UIView *bannerView = [AATKit getPlacementView: self.bannerPlacement];
    CGFloat initialBannerPosX = [[UIScreen mainScreen] bounds].size.width / 2.0;
    CGFloat initialBannerPosY = [[UIScreen mainScreen] bounds].size.height + bannerView.frame.size.height;
    CGPoint initialBannerPos = CGPointMake(initialBannerPosX, initialBannerPosY);
    bannerView.center = initialBannerPos;
    [self.presentingViewController.view addSubview: bannerView];
}


#pragma mark - AATKit delegate methods
- (void) AATKitHaveAd:(id) placement {
    if ( ! self.bannerIsDisplayed) {
        [self animateBannerView];
    }

    [self.delegate AATKitHaveAd: placement];
}

- (void) AATKitNoAds:(id) placement {
    UIView *bannerView = [AATKit getPlacementView: self.bannerPlacement];
    [UIView animateWithDuration:0.5 animations:^{
        CGPoint newPos = CGPointMake(0, bannerView.center.y + bannerView.frame.size.height);
        bannerView.center = newPos;
    } completion:nil];
    [bannerView removeFromSuperview];
    self.bannerIsDisplayed = NO;
    [self.delegate AATKitNoAds: placement];
}

- (void) AATKitShowingEmpty:(id) placement { // ONLY works for banner placements, NOT for fullscreen
    [self.delegate AATKitShowingEmpty: placement];
}

- (void) AATKitPauseForAd {
    [self.delegate AATKitPauseForAd];
}

- (void) AATKitResumeAfterAd {
    [self.delegate AATKitResumeAfterAd];
}

@end
